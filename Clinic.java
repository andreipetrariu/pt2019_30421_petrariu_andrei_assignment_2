package clinic;
import receptionist.Receptionist;
import ui.View;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import doctor.Doctor;
import patient.Patient;

public class Clinic implements Runnable{
	private int minATime;//arrival time range
	private int maxATime;
	private int minTTime;//treatment time range
	private int maxTTime;
	private int nrOfPatients;
	private int simulationTime;
	private Receptionist receptionist;
	private List<Patient> patients;
	private View simulatorView;
	public Clinic(int simulationTime,int nrOfDoctors,int maxPatients,int minATime,int maxATime,int minTTime,int maxTTime,View simulatorView) {
		this.simulationTime=simulationTime;
		simulatorView=new View();
		simulatorView.setVisible(true);
		Random r=new Random();
		this.minATime=minATime;
		this.maxATime=maxATime;
		this.minTTime=minTTime;
		this.maxTTime=maxTTime;
		this.simulatorView=simulatorView;
		nrOfPatients=r.nextInt(maxPatients)+1;
		receptionist=new Receptionist(nrOfDoctors,nrOfPatients,simulatorView);
		patients=new ArrayList<Patient>();
		generatePatients();
	}
	private void generatePatients() {
		int arrivalTime,treatmentTime;
		Random r=new Random();
		for(int i=0;i<nrOfPatients;i++) {
			arrivalTime=r.nextInt(maxATime-minATime +1)+minATime;
			treatmentTime=r.nextInt(maxTTime-minTTime +1)+minTTime;
			patients.add(new Patient(arrivalTime,treatmentTime,i));
		}
		Collections.sort(patients);
	}
	@Override
	public void run() {
		int initialNrOfPatients=nrOfPatients;
		simulatorView.printStatus("Number of patients: "+initialNrOfPatients,0);
		simulatorView.printStatus("Patients:",0);
		for(Patient p:patients) 
			simulatorView.printStatus("ID:"+p.getID()+", arrival time: "+p.getArrivalTime()+", treatment time: "+p.getTreatmentTime(),0);
		simulatorView.printStatus("\n",0);
		int time=0;
		try {
		while(time<simulationTime) {//50 is the time limit
			for(int i=0;i<nrOfPatients;i++) {
				if(patients.get(i).getArrivalTime()==time) {
					receptionist.sendPatient(patients.remove(i));
					nrOfPatients--;
					i--;
				}
			}
			simulatorView.printStatus("Time step: "+time,0);
			simulatorView.initializeStatus(receptionist.getDoctors());
			time++;
			Thread.currentThread().sleep(1000);
		} 
		simulatorView.printStatus("All patients treated! Average waiting time:",initialNrOfPatients);
		} catch (InterruptedException e) {
				e.printStackTrace();
			}
	}
}
