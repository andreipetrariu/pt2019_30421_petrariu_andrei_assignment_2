package receptionist;
import java.util.Iterator;
import java.util.List;
import doctor.Doctor;
import patient.Patient;

public class FastStrategy implements Strategy{

	@Override
	public void addPatient(List<Doctor> doctors, Patient p) {
		int minQueue=10;
		int i=0;
		int doctorNr=0;
		Iterator<Doctor> it=doctors.iterator();
		while(it.hasNext()) {
			Doctor d=(Doctor)it.next();
			if(d.getQueue().size()<minQueue) {
				minQueue=d.getQueue().size();
				doctorNr=i;
			}
			i++;
		}
		try {
		doctors.get(doctorNr).addPatient(p);
	}
		catch(InterruptedException e) {
			System.out.println(e.toString());
		}
	}
}
