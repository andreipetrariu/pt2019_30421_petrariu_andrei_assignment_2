package doctor;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import patient.Patient;
import ui.View;

public class Doctor implements Runnable{
	private BlockingQueue<Patient> queue=new ArrayBlockingQueue<Patient>(100);
	private int queueTime;
	private int id;
	private View simulatorView;
	public Doctor(int id,View simulatorView) {
	queueTime=0;
	this.simulatorView=simulatorView;
	this.id=id;
	}
	public void run() {
		try {
			while(true) {
				Patient p=treatPatient();
				simulatorView.printStatus("\nPatient "+p.getID()+" is being treated by doctor "+ this.id+"! Waiting time "+p.getWaitingTime()+"\n",0);
				simulatorView.addWaitingTime(p.getWaitingTime());
				for(int i=0;i<p.getTreatmentTime();i++) 
					Thread.currentThread().sleep(1000);
				queueTime-=p.getTreatmentTime();
			}
		}
		catch(InterruptedException e) {
			System.out.println(e.toString());
		}
	}
	public BlockingQueue<Patient> getQueue() {
		return queue;
	}
	public int getID() {
		return id;
	}
	public synchronized Patient treatPatient() throws InterruptedException{
		while(queue.size()==0)
			wait();
		Patient p=queue.take();
		notifyAll();
		return p;
	}
	public synchronized void addPatient(Patient p) throws InterruptedException{
		queue.add(p);
		p.setWaitingTime(queueTime);
		queueTime+=p.getTreatmentTime();
		notifyAll();
	}
}
