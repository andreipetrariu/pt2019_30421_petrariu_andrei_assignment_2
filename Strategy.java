package receptionist;
import java.util.List;
import doctor.Doctor;
import patient.Patient;

public interface Strategy {
	public void addPatient(List<Doctor> doctors,Patient p);
}
