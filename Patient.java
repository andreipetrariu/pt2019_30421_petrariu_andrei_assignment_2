package patient;

public class Patient implements Comparable<Patient>{
	private int arrivalTime;
	private int treatmentTime;
	private int waitingTime;
	private int id;
	public Patient(int aTime,int tTime,int id) {
		arrivalTime=aTime;
		treatmentTime=tTime;
		this.id=id;
	}
	public int getWaitingTime() {
		return waitingTime;
	}
	public int getID() {
		return id;
	}
	public void setWaitingTime(int time) {
		waitingTime=time;
	}
	public int getTreatmentTime() {
		return treatmentTime;
	}
	public int getArrivalTime() {
		return arrivalTime;
	}
	@Override
	public int compareTo(Patient p) {
		if(this.getArrivalTime()<p.getArrivalTime())
			return -1;
		else if(this.getArrivalTime()>p.getArrivalTime())
				return 1;
		return 0;
	}
}
