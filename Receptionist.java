package receptionist;
import java.util.*;
import doctor.Doctor;
import patient.Patient;
import ui.View;
public class Receptionist {
	private List<Doctor> doctors;
	private int nrOfDoctors;
	private int nrOfPatients;
	private Strategy strategy;
	public Receptionist(int nrOfDoctors,int nrOfPatients,View simulatorView) {
		this.nrOfDoctors=nrOfDoctors;
		this.nrOfPatients=nrOfPatients;
		strategy=new FastStrategy();
		doctors=new ArrayList<Doctor>();
		for(int i=0;i<nrOfDoctors;i++) {
			doctors.add(new Doctor(i+1,simulatorView));
			Thread t=new Thread(doctors.get(i));
			t.start();
		}
	}
	public void sendPatient(Patient p) {
		strategy.addPatient(doctors, p);
	}
	public List<Doctor> getDoctors(){
		return doctors;
	}
}
