package ui;
import javax.swing.*;

import clinic.Clinic;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import doctor.Doctor;
import patient.Patient;

import java.util.List;
public class View extends JFrame{
	class BtnListener implements MouseListener{
		@Override
		public void mouseClicked(MouseEvent e) {	
			nrOfDoctors=getNrQueues();
			simulationTime=getTime();
			time=0;
			maxPatientsWaiting=0;
			minAT=getMin1();
			minTT=getMin2();
			maxAT=getMax1();
			maxTT=getMax2();
			if(nrOfDoctors>10)
				nrOfDoctors=10;
			int maxPatients=(nrOfDoctors*simulationTime)/maxTT;
			peakTime=0;
			Clinic c=new Clinic(simulationTime,nrOfDoctors,maxPatients,minAT,maxAT,minTT,maxTT,View.this);
			Thread t=new Thread(c);
			startBtn.setEnabled(false);
			t.start();
		}
		@Override
		public void mouseEntered(MouseEvent arg0) {}
		@Override
		public void mouseExited(MouseEvent arg0) {}
		@Override
		public void mousePressed(MouseEvent arg0) {}
		@Override
		public void mouseReleased(MouseEvent arg0) {}
		
	}
	private JTextField minATime,maxATime,minTTime,maxTTime,nrOfQueues,timeTF;
	private JLabel minLabel1=new JLabel("Minimum arriving time",SwingConstants.CENTER);
	private JLabel minLabel2=new JLabel("Minimum treating time",SwingConstants.CENTER);
	private JLabel maxLabel1=new JLabel("Maximum arriving time",SwingConstants.CENTER);
	private JLabel maxLabel2=new JLabel("Maximum treating time",SwingConstants.CENTER);
	private JLabel nrOfQueuesLabel=new JLabel("Number of doctors",SwingConstants.CENTER);
	private JLabel timeLabel=new JLabel("Simulation time",SwingConstants.CENTER);
	private JPanel inputPanel;
	private JPanel statusPanel;
	private JPanel eventsPanel;
	private JScrollPane statusScrollPane;
	private JScrollPane eventsScrollPane;
	private JPanel content;
	private JButton startBtn;
	private int nrOfDoctors;
	private int simulationTime;
	private int minAT;
	private int maxAT;
	private int minTT;
	private int maxTT;
	private int peakTime;
	private int time;
	private int maxPatientsWaiting;
	private int emptyQueueTime;
	private int serviceTime;
	private float totalWaitingTime;
	public View() {
		minATime=new JTextField(2);
		maxATime=new JTextField(2);
		minTTime=new JTextField(2);
		maxTTime=new JTextField(2);
		nrOfQueues=new JTextField(2);
		timeTF=new JTextField(2);
		startBtn=new JButton("START");
		startBtn.addMouseListener(new BtnListener());
		inputPanel=new JPanel(new GridLayout(0,2));
		statusPanel=new JPanel(new GridLayout(0,1));
		statusPanel.setSize(300, 200);
		eventsPanel=new JPanel();
		eventsPanel.setLayout(new BoxLayout(eventsPanel,BoxLayout.Y_AXIS));
		eventsPanel.setSize(500,200);
		statusScrollPane=new JScrollPane(statusPanel,JScrollPane.VERTICAL_SCROLLBAR_NEVER,
	            JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		eventsScrollPane=new JScrollPane(eventsPanel,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
	            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		eventsScrollPane.setLocation(350, 0);
		eventsScrollPane.setSize(500,700);
		inputPanel.setSize(350, 100);
		inputPanel.add(minLabel1);
		inputPanel.add(minATime);
		inputPanel.add(maxLabel1);
		inputPanel.add(maxATime);
		inputPanel.add(minLabel2);
		inputPanel.add(minTTime);
		inputPanel.add(maxLabel2);
		inputPanel.add(maxTTime);
		inputPanel.add(nrOfQueuesLabel);
		inputPanel.add(nrOfQueues);
		inputPanel.add(timeLabel);
		inputPanel.add(timeTF);
		statusScrollPane.setLocation(10, 145);
		statusScrollPane.setSize(330, 200);
		content=new JPanel();
		content.setLayout(null);
		content.add(inputPanel);
		startBtn.setSize(80, 30);
		startBtn.setLocation(135, 105);
		content.add(startBtn);
		content.add(eventsScrollPane);
		content.add(statusScrollPane);
		this.add(content);
		this.setTitle("Hospital Queue Simulation");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(860,710);
		this.setVisible(true);
	}
	public int getMin1() {
		return Integer.valueOf(minATime.getText());
	}
	public int getMin2() {
		return Integer.valueOf(minTTime.getText());
	}
	public int getMax1() {
		return Integer.valueOf(maxATime.getText());
	}
	public int getMax2() {
		return Integer.valueOf(maxTTime.getText());
	}
	public int getNrQueues() {
		return Integer.valueOf(nrOfQueues.getText());
	}
	public int getTime() {
		return Integer.valueOf(timeTF.getText());
	}
	public void addBtnListener(MouseListener l) {
		startBtn.addMouseListener(l);
	}
	public void addWaitingTime(int x) {
		totalWaitingTime+=x;
	}
	public void printStatus(String s,int isFinal) {//isFinal gives the nr of patients and tells the method if the  simulation is over and the waiting time should be printed
		eventsPanel.add(new Label(s));
		if(isFinal!=0) {
			eventsPanel.add(new JLabel((String.valueOf(totalWaitingTime/isFinal))));
			eventsPanel.add(new JLabel("Peak time: "+peakTime));
			eventsPanel.add(new JLabel("Average empty queue time: "+String.valueOf(((float)emptyQueueTime)/((float)nrOfDoctors))));
			eventsPanel.add(new JLabel("Average service time:"+String.valueOf(((float)serviceTime)/((float)nrOfDoctors))));
		}
	    this.validate();
	}
	public void initializeStatus(List<Doctor> doctors) {
		nrOfDoctors=doctors.size();//used in printStatus
		JLabel[] queueLabels=new JLabel[doctors.size()];
		int sumOfPatientsWaiting=0;
		int index=0;
		for(Doctor d:doctors) {
			sumOfPatientsWaiting+=d.getQueue().size();
			index+=1;
			String q="Doctor "+index+": ";
			for(Patient p:d.getQueue())
				q=q+p.getID()+" ";
			queueLabels[index-1]=new JLabel(q);
			if(d.getQueue().isEmpty())
				emptyQueueTime++;
			for(Patient p:d.getQueue())
				serviceTime=serviceTime+p.getTreatmentTime();
		}
		if(sumOfPatientsWaiting>maxPatientsWaiting) {
			maxPatientsWaiting=sumOfPatientsWaiting;
			peakTime=time;
		}
		statusPanel.removeAll();
		for(int i=0;i<index;i++) {
			
			statusPanel.add(queueLabels[i]);
		}
		this.validate();
		time++;
	}
	public static void main(String[] args) {
		View v=new View();
		v.setVisible(true);
	}
}
